﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Level")]

public class Level : ScriptableObject
{
    [SerializeField] private float Rabbitspeed = 1f;
    [SerializeField] private float spawnningSpeed = 1f;
    [SerializeField] private List<string> start = default;

    public List<string> StartText => start;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float getRabbitSpeed()
    {
        return Rabbitspeed;
    }

    public float getspawningSpeed()
    {
        return spawnningSpeed;
    }
}
