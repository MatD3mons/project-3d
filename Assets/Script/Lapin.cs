﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lapin : MonoBehaviour
{
    private SpawnController spawnController = default;
    private LayerMask Filet = default;
    private LayerMask End = default;
    private Vector3 deltaPosition;
    private float speed = 1f;

    private void Awake()
    {
        Filet = LayerMask.NameToLayer("Filet");
        End = LayerMask.NameToLayer("End");
    }

    public void Initalise(float speed, SpawnController spawnController)
    {
        this.spawnController = spawnController;
        this.speed = speed;
    }

    // Start is called before the first frame update
    void Start()
    {
        deltaPosition = new Vector3(0, 0, -1) * speed;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 currentPosition = transform.position;
        transform.position = currentPosition + deltaPosition*Time.deltaTime;
    }
        
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == Filet)
        {
            other.GetComponentInParent<FiletController>().addScore(10f);
            spawnController.remove(gameObject);
            Destroy(gameObject);
        }

        if(other.gameObject.layer == End)
        {
            other.GetComponent<EndController>().removeScore(10f);
            spawnController.remove(gameObject);
            Destroy(gameObject);
        }
    }

}
