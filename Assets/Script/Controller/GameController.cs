﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private SpawnController SpawnController = default;
    [SerializeField] private TextMeshProUGUI ScreenScore = default;

    private LayerMask Player = default;
    private float Score = 0f;

    private void Awake()
    {
        Player = LayerMask.NameToLayer("Player");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == Player)
        {
            SpawnController.setstarting(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(SpawnController.getStarting())
        {
            SpawnController.setstarting(false);
        }
    }

    public void addScore(float i)
    {
        Score = Score + i;
        ScreenScore.text = " Score = " + Score; 
    }

    public void removeScore(float i)
    {
        if (Score - i > 0)
        {
            Score = Score - i;
            ScreenScore.text = " Score = " + Score;
        }
        else
        {
            ScreenScore.text = " Score = 0";
            Debug.Log("Game Over");
            SpawnController.setstarting(false);
        }
    }
}
