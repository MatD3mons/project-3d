﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField] private List<Level> levels = default;
    [SerializeField] private GameObject lapin = default;
    [SerializeField] private Transform spawn1;
    [SerializeField] private Transform spawn2;
    [SerializeField] private Transform spawn3;
    [SerializeField] private Transform spawn4;
    [SerializeField] private TextMeshProUGUI finish;
    [SerializeField] private FiletController filet = default;

    private List<GameObject> lapins = new List<GameObject>();
    private Level level = default;
    private string phase = "0";
    private int vague = 0;
    private bool starting = false;
    private bool vaguing = false;
    private float speedRabbit = 1f;
    private float speedspawn = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void setstarting(bool val)
    {
        starting = val;
    }

    public bool getStarting()
    {
        return starting;
    }

    // Update is called once per frame
    void Update()
    {
        if (starting)
        {
            if (!vaguing && lapins.Count.Equals(0))
            {
                level = levels[vague];
                speedspawn = level.getspawningSpeed();
                speedRabbit = level.getRabbitSpeed();
                StartCoroutine(Apparition());
            }
        }
    }

    public IEnumerator Apparition()
    {
        vaguing = true;
        StartCoroutine(message("Niveau "+(vague+1)));
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < level.StartText.Count; i++)
        {
            if (starting)
            {
                phase = level.StartText[i];
                if (phase.Contains("1"))
                    spawnLapin(spawn1);
                if (phase.Contains("2"))
                    spawnLapin(spawn2);
                if (phase.Contains("3"))
                    spawnLapin(spawn3);
                if (phase.Contains("4"))
                    spawnLapin(spawn4);
                yield return new WaitForSeconds(speedspawn);
            }
            else
            {
                foreach(GameObject l in lapins)
                {
                    Destroy(l);
                    StartCoroutine(message("Niveau Fail !"));
                    vague = 0;
                    vaguing = false;
                }
                lapins.Clear();
            }
        }
        if (starting)
        {
            yield return new WaitForSeconds(10 / speedRabbit);
            StartCoroutine(message("Niveau Terminé !"));
            filet.invisible();
            yield return new WaitForSeconds(4f);
            vaguing = false;
            if (vague < levels.Count-1)
            {
                vague++;
                level = levels[vague];
                speedspawn = level.getspawningSpeed();
                speedRabbit = level.getRabbitSpeed();
                StartCoroutine(Apparition());
            }
            else
            {
                Application.Quit();
                starting = false;
                vague = 0;
            }
        }
    }


    void spawnLapin(Transform transform)
    {
        Vector3 deltaPosition = transform.position;
        Quaternion deltaRotation = transform.rotation;
        GameObject l = Instantiate(lapin, deltaPosition, deltaRotation, transform);
        l.GetComponent<Lapin>().Initalise(speedRabbit,this);
        lapins.Add(l);
    }

    public void remove(GameObject l)
    {
        lapins.Remove(l);
    }

    public IEnumerator message( string message)
    {
        finish.text = message;
        for (int i = 0; i < 100; i++)
        {
            finish.alpha = (float)i / 100;
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(0.75f);
        for (int i = 0; i < 100; i++)
        {
            finish.alpha = (float)(100 - i) / 100;
            yield return new WaitForSeconds(0.005f);
        }
    }

}
