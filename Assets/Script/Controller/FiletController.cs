﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiletController : MonoBehaviour
{
    [SerializeField] private GameController gameController = default;
    [SerializeField] private List<GameObject> lapins = default;
    private int y = 0;
    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void addScore(float i)
    {
        if (y < lapins.Count)
        {
            lapins[y].SetActive(true);
            y++;
        }
        gameController.addScore(i);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void invisible()
    {
        foreach(GameObject g in lapins){
            g.SetActive(false);
        }
        y = 0;
    }
}
