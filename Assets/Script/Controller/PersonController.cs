using UnityEngine;

public class PersonController : MonoBehaviour
{
    [SerializeField] private GameObject filet;
    [SerializeField] private bool isWalking = false;
    [SerializeField] private float walkSpeed = 2f;
    [SerializeField] private float runSpeed = 4f;
    [SerializeField] private float jumpSpeed = 4f;
    [SerializeField] private float gravityMultiplier = 1f;
    [SerializeField] private MouseLook mouseLook;

    private LayerMask Panneau = default;
    private LayerMask Son = default;
    private Camera camera;
    private bool jump;
    private Vector2 input;
    private Vector3 moveDir = Vector3.zero;
    private CharacterController characterController;
    private CollisionFlags collisionFlags;
    private Vector3 originalCameraPosition;

    private void Awake()
    {
        Panneau = LayerMask.NameToLayer("Panneau");
        Son = LayerMask.NameToLayer("Panneau");
    }

    // Use this for initialization
    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        camera = Camera.main;
        originalCameraPosition = camera.transform.localPosition;
        mouseLook.Init(transform, camera.transform);
    }

    // Update is called once per frame
    private void Update()
    {
        mouseLook.LookRotation(transform, camera.transform);
        // the jump state needs to read here to make sure it is not missed
        if (!jump)
            jump = Input.GetButtonDown("Jump");
        ClickRight();
    }

    private void FixedUpdate()
    {
        float speed;
        GetInput(out speed);
        // always move along the camera forward as it is the direction that it being aimed at
        Vector3 desiredMove = transform.forward * input.y + transform.right * input.x;

        // get a normal for the surface that is being touched to move along it
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo,
                           characterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

        moveDir.x = desiredMove.x * speed;
        moveDir.z = desiredMove.z * speed;


        if (characterController.isGrounded)
        {
            if (jump)
            {
                moveDir.y = jumpSpeed;
                jump = false;
            }
        }
        else
            moveDir += Physics.gravity * gravityMultiplier * Time.fixedDeltaTime;

        collisionFlags = characterController.Move(moveDir * Time.fixedDeltaTime);
        mouseLook.UpdateCursorLock();
        movefilet();
    }

    private void movefilet()
    {
        filet.transform.rotation = camera.transform.rotation;
    }

    private void ClickRight()
    {
        if (Input.GetButtonUp("Fire1"))
        {
            Vector3 fwd = camera.transform.TransformDirection(Vector3.forward);
            Vector3 pst = camera.transform.position + fwd * 1f;
            RaycastHit hit;
            if (Physics.Raycast(pst, fwd, out hit, 10))
            {
                if (hit.transform.gameObject.layer == Son)
                {
                    AudioController audioController = hit.transform.gameObject.GetComponent<AudioController>();
                    Debug.Log(audioController);
                    audioController.SoundButtonActivate();
                }
                else if (hit.transform.gameObject.layer == Panneau)
                {
                    Pancarte pancarte = hit.transform.gameObject.GetComponentInParent<Pancarte>();
                    pancarte.go();
                }
            }
        }
    }

    private void GetInput(out float speed)
    {
        // Read input
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        bool waswalking = isWalking;

        isWalking = !Input.GetKey(KeyCode.LeftShift);

        // set the desired speed to be walking or running
        speed = isWalking ? walkSpeed : runSpeed;
        input = new Vector2(horizontal, vertical);

        // normalize input if it exceeds 1 in combined length:
        if (input.sqrMagnitude > 1)
            input.Normalize();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        //dont move the rigidbody if the character is on top of it
        if (collisionFlags == CollisionFlags.Below)
            return;

        if (body == null || body.isKinematic)
            return;

        body.AddForceAtPosition(characterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
    }
}
