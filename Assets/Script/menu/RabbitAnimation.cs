﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitAnimation : MonoBehaviour
{
    private Animator animator;
    private float timer = 10f;
    private bool isDead = false;
    void Awake(){
        animator = GetComponent<Animator>();
        animator.SetBool("isDead", false);
        isDead = false;
    }

    void Start(){
        StartCoroutine(PlayAnimation());
    }

    private IEnumerator PlayAnimation(){
        while(true){
            if(isDead){
                animator.SetBool("isDead", true);
                Destroy(gameObject, 1f);
                isDead = false;
            }
            else{
                timer = Random.Range(8f, 10f);
                isDead = true;
            }
            yield return new WaitForSeconds(timer);
        }
    }
}
