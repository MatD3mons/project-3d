﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitSpawn : MonoBehaviour
{
    [SerializeField] private GameObject RabbitPrefab = default;
    private GameObject rabbitInstance = default;
    private Renderer rend;
    private Color RandomColor;
    private float timer = 0.25f;

    void Awake(){
    }

    void Start()
    {
        StartCoroutine(SpawnRabbit());
    }

    private IEnumerator SpawnRabbit(){
        //Destruction of the instance done in the RabbitAnimation Script.
        while(true){
            rabbitInstance = Instantiate(RabbitPrefab, transform.position, GetRandomRotation());
            //Couleur fonctionne pas à cause du skin de mon prefab
            RandomColor = Random.ColorHSV(0f, 1f);
            //rend = rabbitInstance.GetComponent<Renderer>();
            //rend.material.color = RandomColor;
            foreach(Renderer childRenderer in rabbitInstance.GetComponentsInChildren<Renderer>()){
                    childRenderer.material.color = RandomColor;
                }
            yield return new WaitForSeconds(timer);
        }    
    }

    private static Quaternion GetRandomRotation() {
            return Quaternion.Euler(0f, Random.Range(-90f, 0f), 0f);
    }
}
